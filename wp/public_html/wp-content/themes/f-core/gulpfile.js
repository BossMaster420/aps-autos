const 	gulp = require('gulp');
const 	webpack = require('webpack');
const 	watch = require('gulp-watch');
const 	sass = require('gulp-sass');
		sass.compiler = require('node-sass');
const 	concat = require('gulp-concat');
const 	sourcemaps = require('gulp-sourcemaps');
const 	autoprefixer = require('gulp-autoprefixer');
const 	rename = require('gulp-rename');
const 	jslint = require('gulp-jslint');
const 	uglify = require('gulp-uglify');
const 	browserSync = require('browser-sync').create();
const	sassGlob = require('gulp-sass-glob');
const 	gulpIgnore = require('gulp-ignore');

const 	urlToPreview = 'http://localhost:4444/';
const	lintOptions = {
						"white": true,
						"this": true, 
						"long": true,
						"fudge": true,
						"esversion": 6,
						"unused": false,
						"unparam": true
					}

// function to compile main sass
function compileSass() {
	return gulp
		.src(['./core/sass/**/*.scss', '!./includes/sass/abovethefold.scss'])
		.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(browserSync.stream())
		.pipe(autoprefixer({overrideBrowserslist: ['last 2 versions', '> 5%']}))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./assets/css'))
}

// function to compile blocks sass
function compileBlocksSass() {
	return gulp
		.src(['./components/blocks/**/*.scss'])
		.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(browserSync.stream())
		.pipe(autoprefixer({overrideBrowserslist: ['last 2 versions', '> 5%']}))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./assets/css'))
}

// function to complie abovethefold sass
function compileAbovethefold() {
	return gulp
		.src('./core/sass/abovethefold.scss')
		.pipe(sassGlob())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(browserSync.stream())
		.pipe(autoprefixer({overrideBrowserslist: ['last 2 versions', '> 5%']}))
		.pipe(gulp.dest('./assets/css'))
}

// function to compile page builder js
function compileBlockScripts() {
	return gulp.src('./components/blocks/**/*.js')
		// .pipe(jslint(lintOptions))
		// .pipe(jslint.reporter( 'stylish' ))
		.pipe(uglify())
        .pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('./assets/js'));
}

function compileComponentsScripts() {
	return gulp.src('./components/**/*.js')
		// .pipe(jslint(lintOptions))
		// .pipe(jslint.reporter( 'stylish' ))
		.pipe(concat('blocks.js'))
		.pipe(uglify())
        .pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('./assets/js'));
}

// function to compile libs js
function compileLibScripts() {
	return gulp.src('./core/libs/*.js')
		.pipe(concat('libs.js'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('./assets/js'));
}

function compileBlockMinScript() {
	return gulp.src('./assets/js/**/*.js')
		.pipe(concat('blocks.js'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('./assets/js'));
}

// function to compile main js
function compileMainScript() {
	return gulp.src('./core/js/main.js')
		// .pipe(jslint(lintOptions))
		// .pipe(jslint.reporter( 'stylish' ))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('./assets/js'));
}

// function to compile gutenberg editor js
function compileGutenbergScript() {
	return gulp.src('./core/js/gutenberg.js')
		// .pipe(jslint(lintOptions))
		// .pipe(jslint.reporter( 'stylish' ))
		.pipe(gulp.dest('./assets/js'));
}

// compile everything at once
function compileAll() {
	compileSass();
	compileBlocksSass();
	compileAbovethefold();
    compileBlockScripts();
    compileLibScripts();
	compileMainScript();
	compileGutenbergScript();
	compileBlockMinScript();
}

gulp.task('sass', function() {
	compileSass();
	compileBlocksSass();
	compileAbovethefold();
});

gulp.task('js', function() {
    compileBlockScripts();
    compileLibScripts();
	compileMainScript();
	compileGutenbergScript();
});

gulp.task('compile', function() {
    compileAll();
});

gulp.task('watch', function () {

	browserSync.init({
		notify: false,
		proxy: urlToPreview,
		ghostMode: false
	});

	gulp.watch('./**/*.php', function (done) {
		console.log('php');
		browserSync.reload();
		done();
	});

	gulp.watch('./components/**/*.js', function (done) {
		console.log('blocks js');
	    compileComponentsScripts();
		browserSync.reload();
		done();
	});
	// gulp.watch('./asstes/js/blocks/**/*.min.js', function (done) {
	// 	console.log('blocks 2 js');
	// 	compileBlockMinScript();
	// 	browserSync.reload();
	// 	done();
	// });

	gulp.watch('./core/js/*.js', function (done) {
		console.log('main js');
		compileMainScript();
		compileGutenbergScript();
		browserSync.reload();
		done();
	});

	gulp.watch('./core/libs/*.js', function (done) {
		console.log('lib js');
		compileLibScripts();
		browserSync.reload();
		done();
	});
	

	gulp.watch('./core/**/*.scss', function (done) {
		console.log('main scss');
		compileSass();
		compileBlocksSass();
		compileAbovethefold();
		//  no browserSync required here
		done();
	});

	gulp.watch('./components/blocks/**/*.scss', function (done) {
		console.log('blocks scss');
		compileSass();
		compileBlocksSass();
		compileAbovethefold();
		//  no browserSync required here
		done();
	});

	gulp.watch('./components/**/*.scss', function (done) {
		console.log('blocks scss');
		compileSass();
		compileBlocksSass();
		compileAbovethefold();
		//  no browserSync required here
		done();
	});

});