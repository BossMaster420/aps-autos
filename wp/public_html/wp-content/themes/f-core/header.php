<?php if ( !class_exists( 'acf' ) ): ?>
    <p style="text-align: center;">Please activate Advanced Custom Fields</p>
<?php die; endif; ?>

<!DOCTYPE html>
<html id="html" lang="en">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Author" content="Fiducia Design">
<!--[if lte IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/ie.css" rel="stylesheet">
<![endif]-->

<!-- noptimize -->
<!-- Google Tag Manager -->
<!-- End Google Tag Manager -->
<!--/noptimize-->


<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://ajax.googleapis.com">

<script>
    ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>

<?php if(!DEBUG_MODE) : ?>
	<style>
	<?php echo file_get_contents(get_template_directory() . '/assets/css/abovethefold.css'); ?>
	</style>
<?php endif; ?>

<link rel="apple-touch-icon" sizes="180x180" href="/aps/wp-content/themes/f-core/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/aps/wp-content/themes/f-core/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/aps/wp-content/themes/f-core/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#eba70a">
<meta name="msapplication-TileColor" content="#eba70a">
<meta name="theme-color" content="#eba70a">
<link rel="manifest" href="/aps/wp-content/themes/f-core/manifest.json">


<script>
	// Font preloader - hide text until font has been loaded
	document.getElementById('html').className += ' wf-loading ';
	WebFontConfig = {
		google: { 
			families: [ 
				'Montserrat:400,700&display=swap'
			]
		}
	};
	(function() {
		var wf = document.createElement('script');
		wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		wf.type = 'text/javascript';
		wf.async = 'true';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(wf, s);
	})();
</script>



<?php wp_head(); ?>
<!-- <script src="<?php echo get_template_directory_uri(); ?>/assets/js/modernizr.min.js"></script> -->

</head>

<body <?php body_class(get_field('theme_theme_select')); ?>>

<?php include_header_footer('header'); ?>