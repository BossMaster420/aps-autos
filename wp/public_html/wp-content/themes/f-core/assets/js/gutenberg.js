// set the global variables here for JSLint
/*global AOS, jQuery, document, wp, console, setTimeout, window */
"use strict";

// set fullscreen mode by default
jQuery(document).ready(function($){

    var isFullScreenMode = wp.data.select('core/edit-post').isFeatureActive('fullscreenMode');
    if ( !isFullScreenMode ) {
        wp.data.dispatch('core/edit-post').toggleFeature('fullscreenMode');
    }

    jQuery('#acf-field_60366427e98a2').ready(function(e) {

        var pageTheme = jQuery('#acf-field_60366427e98a2').val();
        var previousTheme;

        jQuery('#acf-field_60366427e98a2').change(function(e) {
            pageTheme = jQuery('#acf-field_60366427e98a2').val();
            jQuery('.block-editor-block-list__layout').removeClass(previousTheme);
            jQuery('.block-editor-block-list__layout').addClass(pageTheme);
            
            console.log(previousTheme);
            console.log(pageTheme);
            
            previousTheme = pageTheme;
            
        })
           
        previousTheme = pageTheme;
        jQuery('.block-editor-block-list__layout').addClass(pageTheme);
    });
});
