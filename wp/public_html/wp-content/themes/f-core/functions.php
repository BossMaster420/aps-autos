<?php 
// Theme Options
CONST HEADER_TYPE = 1;
CONST FOOTER_TYPE = 1;
CONST DEBUG_MODE = 1;

include 'functions/setup_gutenberg.php';
include 'functions/setup_headers_footers.php';
include 'functions/setup_acf.php';
include 'functions/setup_autoptimize.php';
include 'functions/setup_shortcodes.php';
include 'functions/setup_theme.php';
include 'functions/setup_upload_resize.php';
include 'functions/setup_images.php';
include 'functions/setup_restrict.php';
include 'functions/setup_gallery.php';

include 'components/blocks/register-blocks.php';