<?php get_header(); ?>

<?php if(DEBUG_MODE): ?>

    <?php wp_enqueue_style( 'block-acf-cta-blocks' , get_template_directory_uri() . '/assets/css/cta-blocks/cta-blocks.css' ); ?>

<?php else: ?>

    <style><?php echo file_get_contents(get_template_directory() . '/assets/css/cta-blocks/cta-blocks.css'); ?></style>

<?php endif; ?>

<div class="cta-blocks mt-20">

    <section class="block <?= esc_attr($class_name) ?>" data-aos="fade-up">

        <?php if(DEBUG_MODE): ?>

            <?php wp_enqueue_style( 'block-acf-' . $block_name , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>

        <?php else: ?>

            <?php if( !is_admin() && $block['id'] === get_first_block_id() || !is_admin() && $block['id'] === get_second_block_id() ): ?>
                <style><?php echo file_get_contents(get_template_directory() . '/assets/css/'.$block_name.'/'.$block_name.'.css'); ?></style>
            <?php else: ?>
                <?php wp_enqueue_style( 'block-acf-'.$block_name.'-block' , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>
            <?php endif; ?>

        <?php endif; ?>

        <?php if( $theme['theme'] == 'theme--image' ): ?>
            <div class="banner-images-wrapper">
                <div class="banner-image">         
                    <?php echo img_sizes($theme['image'], ['default' => 'img_1920', 'page_area' => '100', 'tablet_page_area' => '100', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>               
                </div>
            </div>
        <?php endif; ?>
        
        <header class="content text-center">
            <?php
            if($display_logo) {

                if($logo_type == 'svg') {
                    echo $logo;
                }
                if($logo_type == 'image') {
                    echo img_sizes($logo, ['default' => 'img_375', 'page_area' => '10', 'mobile_page_area' => '20', 'lazy_load' => false, 'class' => 'logo']);
                }
            }
            ?>

            <?= $text['title']    ? '<h1  >'     . $text['title']    .'</h1>' : ''?>
            <?= $text['subtitle'] ? '<p class="subtitle">'  . $text['subtitle'] .'</p>'  : ''?>
            <?= $text['caption']  ? '<p class="caption">'   . $text['caption']  .'</p>'  : ''?>
        </header>

    </section>

<div class="in-page-nav-wrap mb-8">
    <div class="in-page-nav js-category-nav-blog flex flex-wrap justify-center text-center">
        <a href="#" class="active" data-cat="all">All</a>
        <?php $categories = get_categories() ?>
        
        <?php foreach( $categories as $category): ?>
            <a href="#" data-cat="<?= $category->slug ?>"><?= $category->name ?></a>
        <?php endforeach; ?>

    </div>
</div>

<div class="cta-blocks-wrapper flex content-center items-center justify-center flex-wrap">
        <?php $delay = 0 ?>
<?php while ( have_posts() ) : the_post();?>
    <?php 

    $id         = get_the_id();
    $image      = get_field('featured_image',   $id);
    $title      = get_field('featured_title',   $id);
    $caption    = get_field('featured_caption', $id);
    $category   = get_the_category();
    $link       = get_the_permalink();
    
    ?>
    <div class="cta-wrapper <?php foreach($category as $category) { echo $category->slug.' '; } ?>" data-aos="fade-up" data-aos-delay="<?= $delay; $delay += 100 ?>">
        <?php echo img_sizes($image, ['default' => 'img_800', 'page_area' => '25', 'tablet_page_area' => '25', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>
        <div class="cta text-center flex content-center items-center theme--image smoke-top">

            <h3 class="h2 no-margin font-primary"> <?= $title ?>
                <p class="caption mt-2"> <?= $caption ?></p>
            </h3>
            <a href="<?= $link ?>" class="button no-margin primary theme--default">Read more</a>

        </div>
    </div>
<?php endwhile; ?>
    </div>
</div>

<?php get_footer(); ?>