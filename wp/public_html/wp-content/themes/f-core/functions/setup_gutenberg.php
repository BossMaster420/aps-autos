<?php

/**
 * Add UP Core Blocks as a block category
 */

function fcore_block_category( $categories, $post ) {
	return array_merge(
		array(
			array(
				'slug' => 'fcore-blocks',
				'title' => __( 'F Core Blocks', 'f-core' ),
			),
		),
		array(
			array(
				'slug' => 'banners',
				'title' => __( 'Banners', 'banners' ),
			),
        ),
        $categories
	);
}
add_filter( 'block_categories', 'fcore_block_category', 10, 2);


/**
 * Activate wide alignment options on blocks
 */

function mytheme_setup_theme_supported_features() {
    add_theme_support( 'align-wide' );
}
add_action( 'after_setup_theme', 'mytheme_setup_theme_supported_features' );


/**
 * Enqueue block JavaScript and CSS for backend gutenberg editor
 */
if (is_admin()){
function block_plugin_editor_scripts() {

    print "
        <script>
            // Font preloader - hide text until font has been loaded
            document.getElementsByTagName('html').className += ' wf-loading ';
            WebFontConfig = {
                google: { 
                    families: [ 
                        // 'Inter:400,700&display=swap'
                        'Great+Vibes:400',
                        'Raleway:400,500,800&display=swap'
                    ]
                }
            };
            (function() {
                var wf = document.createElement('script');
                wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
                wf.type = 'text/javascript';
                wf.async = 'true';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(wf, s);
            })();
        </script>";
	
    // Enqueue libs JS
    wp_enqueue_script(
        'libs-js',
        get_template_directory_uri() . '/assets/js/libs.min.js',
        [ 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-components', 'wp-editor' ],
        null
    );

    // Enqueue main JS
    wp_enqueue_script(
        'main-js',
        get_template_directory_uri() . '/assets/js/main.min.js',
        [ 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-components', 'wp-editor' ],
        null
    );

    // Enqueue gutenberg JS
    wp_enqueue_script(
        'block-editor-js',
        get_template_directory_uri() . '/assets/js/gutenberg.js',
        [ 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-components', 'wp-editor' ],
        null
    );

    // Enqueue blocks JS
    wp_enqueue_script(
        'blocks-js',
        get_template_directory_uri() . '/assets/js/blocks.min.js',
        [ 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-components', 'wp-editor' ],
        null
    );

    // Enqueue editor styles
    wp_enqueue_style(
        'block-editor-css',
        get_template_directory_uri() . '/assets/css/blocks-editor.css',
        [ 'wp-edit-blocks' ],
        null
    );

    // // Enqueue custom blocks styles
    // wp_enqueue_style(
    //     'blocks-css',
    //     get_stylesheet_directory_uri() . '/assets/css/blocks.css',
    //     [ 'wp-edit-blocks' ],
    //     null
    // );

    // Enqueue global styles
    // wp_enqueue_style(
    //     'global-css',
    //     get_stylesheet_directory_uri() . '/assets/css/global.css',
    //     [ 'wp-edit-blocks' ],
    //     null
    // );
}

// Hook the enqueue functions into the editor
add_action( 'admin_enqueue_scripts', 'block_plugin_editor_scripts' );
}

add_theme_support( 'editor-styles' );


// remove some default inline css
add_filter('block_editor_settings', function ($editor_settings) {
    unset($editor_settings['styles'][0]);

    return $editor_settings;
});

function my_remove_gutenberg_styles($translation, $text, $context, $domain)
{
    if($context != 'Google Font Name and Variants' || $text != 'Noto Serif:400,400i,700,700i') {
        return $translation;
    }
    return 'off';
}
add_filter( 'gettext_with_context', 'my_remove_gutenberg_styles',10, 4);


// disable custom font sizes
add_theme_support( 'disable-custom-font-sizes' );

// disable custom colours and gradients
add_theme_support( 'disable-custom-colors' );
add_theme_support( 'disable-custom-gradients' );

// remove patterns
remove_theme_support( 'core-block-patterns' );