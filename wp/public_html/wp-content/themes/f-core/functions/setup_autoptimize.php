<?php

/**
* @desc check if autoptomize and supercache is enabled
*/
if( strpos($_SERVER['HTTP_HOST'], 'localhost') == false ) {
    function check_super_cache() {
        if ( is_admin() ) {
            if ( !is_plugin_active( 'autoptimize/autoptimize.php' ) ) {
                ?>
                <div class="error notice">
                    <p><?php _e( 'Autoptimise is deactivated! If you have disabled this by purpose please dont forget to activate it after.', 'dashboard' ); ?></p>
                </div>
                <?php
            }
            if ( !is_plugin_active( 'wp-super-cache/wp-cache.php' ) ) {
                ?>
                <div class="error notice">
                    <p><?php _e( 'Super-Cache is deactivated! If you have disabled this by purpose please dont forget to activate it after.', 'dashboard' ); ?></p>
                </div>
                <?php
            }
        }
    }
    add_action( 'admin_notices', 'check_super_cache' );
    add_action( 'wp_footer', 'check_super_cache' );
}