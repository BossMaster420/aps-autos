<?php 

/**
* @desc Register Menus Based on header type
*/
if ( HEADER_TYPE == 1 ) {
   register_nav_menu( 'Main Menu', 'Main Menu' );
   register_nav_menu( 'Header Menu', 'Header Menu' );
}

/**
* @desc Register Menus Based on footer type
*/
if ( FOOTER_TYPE == 1 ) {
   register_nav_menu( 'Footer Menu 1', 'Footer Menu 1' );
   register_nav_menu( 'Footer Menu 2', 'Footer Menu 2' );
   register_nav_menu( 'Footer Menu 3', 'Footer Menu 3' );
   register_nav_menu( 'Copyright'    , 'Copyright'     );
}

/**
* @desc Include Header files across set locations
* current locations are: header, footer
*/
function include_header_footer($location) {
   ////////////////////////
   // Headers
   ////////////////////////
   if(HEADER_TYPE === 1) {
      if($location == 'header') {
         include get_stylesheet_directory().'/components/header-1/header.php';
      }
      if($location == 'footer') {
         include get_stylesheet_directory().'/components/header-1/footer.php';
      }
   }
   ////////////////////////
   // Footers
   ////////////////////////
   if(FOOTER_TYPE == 1) {
      if($location == 'header') {
  
      }
      if($location == 'footer') {
         include get_stylesheet_directory().'/components/footer-1/footer.php';
      }
   }

}