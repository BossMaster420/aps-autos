<?php

/**
 * @desc deregister old jQuery with vulnerabilities and replace with new
 */
function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    wp_deregister_script( 'jquery-migrate' );
    wp_register_script( 'jquery', "/wp-content/themes/f-core/assets/js/jquery-3.6.0.min.js", array(), '3.6.0' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

/**
* @desc add jQuery to head
*/
function insert_jquery(){
    wp_enqueue_script('jquery', false, array(), false, false);
}
add_filter('wp_enqueue_scripts','insert_jquery',1);

/**
 * @desc rename blog title
 */
function rename_stupid_title(){
    $currentTitle = get_option( 'blogdescription' );
    if($currentTitle == 'Just another WordPress site'){
        update_option( 'blogdescription', 'Site in development');
    }
}
add_action( 'after_switch_theme', 'rename_stupid_title' );
add_action( 'wpmu_new_blog', 'rename_stupid_title' );

