<?php 
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Site Options');
}


/**
* @desc auto set licenese key
*/
function acf_auto_set_license_keys() {
  if (!get_option('acf_pro_license')) {
    $save = array(
        'key'	=> 'b3JkZXJfaWQ9NjYxNjZ8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE1LTEwLTA5IDEzOjIzOjI1',
        'url'	=> home_url()
    );
    $save = maybe_serialize($save);
    $save = base64_encode($save);
    update_option('acf_pro_license', $save);
  }
}
add_action('after_switch_theme', 'acf_auto_set_license_keys');

/**
* @desc change dev key when go live
*/
function acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyCb4yfZhPvS3pTzhiVUR3E5jZ7UHNF1HZc';
	return $api;
}
add_filter('acf/fields/google_map/api', 'acf_google_map_api');

function up_load_icon_choices($field) {
    $path = __DIR__ . '/' . '../autoload-svgs/';
    $field['choices'] = array();

    if(!is_dir($path)){
        return $field;
    }

    $svgsUnprocessed = scandir($path);

    $svgs = array_filter($svgsUnprocessed, function($svg){
        return (substr($svg, -4) == '.svg');
    });

    foreach($svgs as $svg){
        $friendlyName = basename($svg, '.svg');
        $svgContents = file_get_contents($path . $svg);
        $field['choices'][ $svgContents ] = $friendlyName;
    }

    return $field;
}
add_filter('acf/load_field/name=autoloaded_icon', 'up_load_icon_choices');

/**
 * Get ID of the first and second ACF block on the page
 */
function get_first_block_id() {
    $post = get_post(); 

    if(has_blocks($post->post_content)) {
        $blocks = parse_blocks($post->post_content);

        $cleanedBlocks = array_filter($blocks, function($block){
            return $block['blockName'];
        });
        
        // rebase count
        $cleanedBlocks = array_values($cleanedBlocks);

        $first_block_attrs = $cleanedBlocks[0]['attrs'];

        if( array_key_exists('id', $first_block_attrs) ) {
            return $first_block_attrs['id'];
        }
    }
}
function get_second_block_id() {
    $post = get_post(); 

    if(has_blocks($post->post_content)) {
        $blocks = parse_blocks($post->post_content);

        $cleanedBlocks = array_filter($blocks, function($block){
            return $block['blockName'];
        });
        
        // rebase count
        $cleanedBlocks = array_values($cleanedBlocks);

        $second_block_attrs = $cleanedBlocks[1]['attrs'];

        if( @array_key_exists('id', $second_block_attrs) ) {
            return $second_block_attrs['id'];
        }
    }
}