// Main.js

// fixes "Does not use passive listeners to improve scrolling performance" from PageSpeed Insights
jQuery.event.special.touchstart = {
    setup: function( _, ns, handle ){
        this.addEventListener("touchstart", handle, { passive: true });
    }
};

// service worker for fake Progressive Web App
if (navigator.hasOwnProperty('serviceWorker')) {
    window.addEventListener('load', function() {
        navigator.serviceWorker.register('/service-worker.js')
        .then(function(registration) {
            registration.update();
        })
        .catch(function(error) {
            console.log('Registration failed with ' + error);
        });
    });
}

jQuery(function($){

    $('.js-nav-toggle').click(function(e) {
        jsNavToggle(e);
    });

    function jsNavToggle(e) {
        e.preventDefault();
        $('.js-nav-toggle').toggleClass("active");
        $('.overlay-menu').toggleClass("active")
    }

    $('p:empty').remove(); // Hack to remove WP generated empty p tags

    headerScroll();

    AOS.init();
    AOS.refresh();

    $('.js-category-nav-blog a').on('click', function(e) {
        e.preventDefault()
        $category = $(this).attr('data-cat');
        $(this).parent().children().removeClass('active');
        $(this).addClass('active');

        if($category != $activeCategory) {
            updatePosts($category);
        }

        function updatePosts($category) {
            $('.cta-blocks-wrapper').children().fadeOut( 'fast', 'swing', function() { fadeInPosts($category) } );
    
            function fadeInPosts($category) {
                if($category == 'all') {
                    $('.cta-blocks-wrapper').children().fadeIn( 'fast', 'swing', function(){ AOS.refresh(); } );
                } else {
                    console.log($category)
                    $('.cta-blocks-wrapper').find('.'+$category).fadeIn('slow', 'swing', function(){ AOS.refresh(); } );
                }
            }
        }
    })
})

