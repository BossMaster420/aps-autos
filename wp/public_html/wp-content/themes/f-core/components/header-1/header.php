<?php 

$buttons = get_field('header_buttons_buttons', 'options');

$logo_type = get_field('logo_type', 'options');

if($logo_type == 'svg') {
    $logo = get_field('logo_svg', 'options');
}
if($logo_type == 'image') {
    $logo = get_field('logo_image', 'options');
}

?>

<header class="header-1 scrolled hidden">
    <div class="wrapper">
        <div class="header-left flex justify-center items-center">   
            <a href="<?= get_home_url() ?>" class="logo <?= is_front_page() ? 'home-page' : ''; ?>">
                <?php
                if($logo_type == 'svg') {
                    echo $logo;
                }
                if($logo_type == 'image') {
                    echo img_sizes($logo, ['default' => 'img_375', 'page_area' => '10', 'mobile_page_area' => '20', 'lazy_load' => false, 'class' => 'logo']);
                }
                ?>
            </a>
        </div>

        <div class="header-right flex justify-end items-center">
            <?php wp_nav_menu( array( 'theme_location' => 'Header Menu' , 'container' => false, 'menu_class' => 'list-reset hide-tablet' ) ); ?>
            
            <?php if($buttons): foreach($buttons as $button): $button = $button['button'];?>
                <a href="<?= $button['button_link'] ?>" class="hide-mobile button <?= $button['button_style'] ?>" <?= ($button['external'] ? ' target="_blank" rel="noopener" ' : '') ?> > <?= $button['button_text'] ?> </a>
            <?php endforeach; endif; ?>

            <a href="#" class="nav-toggle js-nav-toggle">
                <span></span>
                <span></span>
                <span></span>
            </a>

        </div>
    </div>

</header>