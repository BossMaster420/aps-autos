
(function($) {
    // change header on scroll

    var subMenu = false
    $(document).scroll(function() {
        if(!subMenu) {
            headerScroll();
        }
    });
    $(window).resize(function() {
        headerScroll();
    });

    // // Hide Header on on scroll down
    // var didScroll;
    // var lastScrollTop = 0;
    // var delta = 5;
    // var navbarHeight = $('header.header-1').outerHeight();

    // $(window).scroll(function(event){
    //     didScroll = true;
    // });

    // setInterval(function() {
    //     if (didScroll && !subMenu) {
    //         hasScrolled();
    //         didScroll = false;
    //     }
    // }, 250);

    // function hasScrolled() {
    //     var st = $(this).scrollTop();
        
    //     // Make sure they scroll more than delta
    //     if(Math.abs(lastScrollTop - st) <= delta)
    //         return;
        
    //     // If they scrolled down and are past the navbar, add class .nav-up.
    //     // This is necessary so you never see what is "behind" the navbar.
    //     if (st > lastScrollTop && st > navbarHeight){
    //         // Scroll Down
    //         $('header.header-1').removeClass('nav-down').addClass('hidden');
    //     } else {
    //         // Scroll Up
    //         if(st + $(window).height() < $(document).height()) {
    //             $('header.header-1').removeClass('nav-up').removeClass('hidden');
    //         }
    //     }
        
    //     lastScrollTop = st;
    // }

    // $('.header-1 .menu-item-has-children > a').click(function(e) {
    //     e.preventDefault();
    //     subMenu = !subMenu;
    //     $(this).parent().toggleClass('active');
    //     $(this).parent().find('.sub-menu').toggleClass('active');
    // });

    // $('.overlay-menu .menu-item-has-children > a').click(function(e) {
    //     e.preventDefault();
    //     if($(this).parent().hasClass('active')) {
    //         $(this).parent().removeClass('active');
    //         $(this).parent().find('.sub-menu').toggleClass('active');
    //         $(this).parent().find('.sub-menu').slideUp();
    //     } else {
    //         var active = $('.overlay-menu').find('.menu-item-has-children.active');
    //         active.parent().removeClass('active');
    //         active.parent().find('.sub-menu').slideUp()
    //         $(this).parent().addClass('active');
    //         $(this).parent().find('.sub-menu').addClass('active');
    //         $(this).parent().find('.sub-menu').slideDown();
    //     }
    // })

    // $('.sub-menu.active .menu-item').click(function(e) {
    //     console.log('clicked');
    //     window.open(this.href);
    // });
})( jQuery );

function headerScroll(){
    (function($) {
        $(window).on('scroll', function(){
            var scrollTop = $(window).scrollTop();
            if (scrollTop > 500) {
                $('header.header-1').addClass('scrolled');
                $('header.header-1').addClass('theme--accent');
                $('header.header-1').addClass('hidden');
            } else {
                $('header.header-1').removeClass('scrolled');
                $('header.header-1').removeClass('theme--accent');
                $('header.header-1').removeClass('hidden');
            }
        });
    })( jQuery );
}
