<div class="overlay-menu">
    <div class="logo">
        <a href="<?= get_home_url() ?>" class="logo h3">
            APS Autos
        </a>
    </div>
    <a href="#" class="nav-toggle js-nav-toggle">
        <span></span>
        <span></span>
        <span></span>
    </a>
    <div class="menu-wrapper">

        <?php wp_nav_menu( array( 'theme_location' => 'Main Menu' , 'container' => false, 'menu_class' => 'list-reset' ) ); ?>
    </div>
</div>