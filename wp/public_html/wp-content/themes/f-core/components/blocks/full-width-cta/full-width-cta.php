<?php 

/**
 * Full Width CTA
 *
 * This is the template that displays the 'Full Width CTA' block.
 * 
 * Occuraces to change
 * Full Width CTA
 * full-width-cta
 * full_width_cta
 */

acf_register_block_type(array(
    'name'              => 'Full Width CTA',
    'title'             => __('Full Width CTA'),
    'description'       => __('Full Width CTA Block'),
    'render_callback'   => 'full_width_cta_render_callback',
    'category'          => 'fcore-blocks',
    'icon'              => 'align-full-width',
    'keywords'          => array( 'cta', 'block', 'call', 'to', 'action', 'full', 'width' ),
    'supports'          => [ 'align' => false, 'align_text' => true ]
));


function full_width_cta_render_callback( $block, $content = '', $is_preview = false, $post_id = 0 ) {
    /////////////////////////////////////////////////////////////////////////////
    // Base ACF Fields
    $block_name     = 'full-width-cta'; //Make Sure its the same as folder name
    $theme          =  get_field( 'theme_select_theme_select' );
    //////////////////////////////////////////////////////////////////////////////////
    // Block Related ACF Fields
    $text           =  get_field( 'content_text' );
    //////////////////////////////////////////////////////////////////////////////////

    $buttons        = get_field( 'buttons_buttons' );
    //////////////////////////////////////////////////////////////////////////////////
    // Base Classes & Generate Names & ID's
    $class_name  = $block_name .' ';
    $class_name .= 'container ';
    //////////////////////////////////////////////////////////////////////////////////
    // Block Related Classes

    //////////////////////////////////////////////////////////////////////////////////
   ?>
   <section class="block <?= $theme['theme'] ?> ">
    <section class="<?= esc_attr($class_name) ?>" data-aos="fade-up">

    <?php if(DEBUG_MODE): ?>

        <?php wp_enqueue_style( 'block-acf-' . $block_name , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>

    <?php else: ?>

        <?php if( !is_admin() && $block['id'] === get_first_block_id() || !is_admin() && $block['id'] === get_second_block_id() ): ?>
            <style><?php echo file_get_contents(get_template_directory() . '/assets/css/'.$block_name.'/'.$block_name.'.css'); ?></style>
        <?php else: ?>
            <?php wp_enqueue_style( 'block-acf-'.$block_name.'-block' , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>
        <?php endif; ?>

    <?php endif; ?>

    <?php if( $theme['theme'] == 'theme--image' ): ?>
        <div class="bg-images-wrapper">
            <div class="bg-images">
                <?php echo img_sizes($theme['image'], ['default' => 'img_1920', 'page_area' => '100', 'tablet_page_area' => '100', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>               
            </div>
        </div>
    <?php endif; ?>

        <div class="cta-wrapper">

        <?php if( $text['title'] || $text['subtitle'] || $text['caption'] ): ?>
        <div class="content <?= $block['align_text'] ? 'text-' . $block['align_text'] : '' ?>">
            <header data-aos="fade-up">
                <?= $text['title']    ? '<h2      data-aos="fade-up" data-aos-delay="0">'  . $text['title']    .'</h1>' : '' ?>
                <?= $text['subtitle'] ? '<p class="subtitle" data-aos="fade-up" data-aos-delay="100">'  . $text['subtitle'] .'</p>'  : '' ?>
            </header>
            <?= $text['caption']  ? '<div class="caption" data-aos="fade-up" data-aos-delay="200" >'   . $text['caption']  .'</div>'  : '' ?>
            <?php if( $buttons ): ?>
                <div class="buttons theme--default <?= $block['align_text'] ? 'flex justify-' . $block['align_text'] : '' ?>">
                <?php $delay = 200; ?>
                <?php foreach( $buttons as $button ): $button = $button['button']?>
                    <?php $button['external'] ? $button['external'] = ' target="_blank" rel="noopener" ' : '' ?>
                    <a href="<?= $button['button_link'] ?>" <?= $button['external'] ?>class="button <?= $button['button_style'] ?>" data-aos="fade-up" data-aos-delay="<?= $delay; $delay += 50 ?>"><?= $button['button_text'] ?></a>
                <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

        </div>
    </section>
    </section>

<?php } ?>