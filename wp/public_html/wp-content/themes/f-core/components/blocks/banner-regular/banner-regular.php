<?php 

/**
 * Banner - Regular
 *
 * This is the template that displays the 'Banner - Regular' block.
 */

acf_register_block_type(array(
    'name'              => 'Banner - Regular',
    'title'             => __('Banner - Regular'),
    'description'       => __('Regular page banner'),
    'render_callback'   => 'banner_regular_render_callback',
    'category'          => 'fcore-blocks',
    'icon'              => 'cover-image',
    'keywords'          => array( 'banner', 'default', 'regular' ),
    'supports'          => [ 'align' => false ]
));


function banner_regular_render_callback() {
    $block_name  = 'banner-regular'; //Make Sure its the same as folder name
    $text        =  get_field('text_text');
    $banner_size = get_field('banner_size');
    // ->$text['title']
    // ->$text['subtitle']
    // ->$text['caption']
    $theme      = get_field('theme_theme_select');
    
    $logo_type = get_field('logo_type', 'options');
    $display_logo = get_field('display_logo');
    if($logo_type == 'svg') {
        $logo = get_field('logo_svg', 'options');
    }
    if($logo_type == 'image') {
        $logo = get_field('logo_image', 'options');
    }
        // ->$theme['theme']
    // ->$theme['video']
    // ->$theme['images']
    // ->-> $theme['image']

    //Generate Names & ID's
    $class_name  = $block_name .' ';
    $class_name .= $theme['theme'] . ' ';
    $class_name .= $banner_size . ' ';
    $class_name .= 'container ';
    $class_name .= 'mb-4 ';
   ?> 
    <section class="block <?= esc_attr($class_name) ?>">

        <?php if(DEBUG_MODE): ?>

            <?php wp_enqueue_style( 'block-acf-' . $block_name , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>

        <?php else: ?>

            <?php if( !is_admin() && $block['id'] === get_first_block_id() || !is_admin() && $block['id'] === get_second_block_id() ): ?>
                <style><?php echo file_get_contents(get_template_directory() . '/assets/css/'.$block_name.'/'.$block_name.'.css'); ?></style>
            <?php else: ?>
                <?php wp_enqueue_style( 'block-acf-'.$block_name.'-block' , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>
            <?php endif; ?>

        <?php endif; ?>

        <?php if( $theme['theme'] == 'theme--image' ): ?>
            <div class="banner-images-wrapper">
                <div class="banner-image">         
                    <?php echo img_sizes($theme['image'], ['default' => 'img_1920', 'page_area' => '100', 'tablet_page_area' => '100', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>               
                </div>
            </div>
        <?php endif; ?>
        
        <header class="content text-center">
            <?php
            if($display_logo) {

                if($logo_type == 'svg') {
                    echo $logo;
                }
                if($logo_type == 'image') {
                    echo img_sizes($logo, ['default' => 'img_375', 'page_area' => '10', 'mobile_page_area' => '20', 'lazy_load' => false, 'class' => 'logo']);
                }
            }
            ?>

            <?= $text['title']    ? '<h1>'     . $text['title']    .'</h1>' : ''?>
            <?= $text['subtitle'] ? '<p class="subtitle">'  . $text['subtitle'] .'</p>'  : ''?>
            <?= $text['caption']  ? '<p class="caption">'   . $text['caption']  .'</p>'  : ''?>
        </header>

    </section>
    <?php
}

