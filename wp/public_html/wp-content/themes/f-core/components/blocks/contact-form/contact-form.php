<?php 

/**
 * Contact Form
 *
 * This is the template that displays the 'Contact Form' block.
 * 
 * Occuraces to change
 * Contact Form
 * contact-form
 * contact_form
 */

acf_register_block_type(array(
    'name'              => 'Contact Form',
    'title'             => __('Contact Form'),
    'description'       => __('Contact Form Block'),
    'render_callback'   => 'contact_form_render_callback',
    'category'          => 'fcore-blocks',
    'icon'              => 'align-full-width',
    'keywords'          => array( 'cta', 'block', 'call', 'to', 'action', 'full', 'width' ),
    'supports'          => [ 'align' => false, 'align_text' => true ]
));


function contact_form_render_callback( $block, $content = '', $is_preview = false, $post_id = 0 ) {
    /////////////////////////////////////////////////////////////////////////////
    // Base ACF Fields
    $block_name     = 'contact-form'; //Make Sure its the same as folder name
    $theme          =  get_field( 'theme_select_theme_select' );
    //////////////////////////////////////////////////////////////////////////////////
    // Block Related ACF Fields
    $contact_form   = get_field( 'contact_form' );
    $text           = get_field( 'content_text' );
    $buttons        = get_field( 'buttons_buttons' );

    $address = get_field('address', 'options');
    $county = get_field('county', 'options');
    $post_code = get_field('post_code', 'options');
    $tel = get_field('tel', 'options');
    $email = get_field('email', 'options');
    //////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////
    // Base Classes & Generate Names & ID's
    $class_name  = $block_name .' ';
    $class_name .= 'container ';
    //////////////////////////////////////////////////////////////////////////////////
    // Block Related Classes

    //////////////////////////////////////////////////////////////////////////////////
   ?>
   <section class="block <?= $theme['theme'] ?> ">
    <section class="<?= esc_attr($class_name) ?>" data-aos="fade-up">

    <?php if(DEBUG_MODE): ?>

        <?php wp_enqueue_style( 'block-acf-' . $block_name , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>

    <?php else: ?>

        <?php if( !is_admin() && $block['id'] === get_first_block_id() || !is_admin() && $block['id'] === get_second_block_id() ): ?>
            <style><?php echo file_get_contents(get_template_directory() . '/assets/css/'.$block_name.'/'.$block_name.'.css'); ?></style>
        <?php else: ?>
            <?php wp_enqueue_style( 'block-acf-'.$block_name.'-block' , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>
        <?php endif; ?>

    <?php endif; ?>

    <?php if( $theme['theme'] == 'theme--image' ): ?>
        <div class="bg-images-wrapper">
            <div class="bg-images">
                <?php echo img_sizes($theme['image'], ['default' => 'img_1920', 'page_area' => '100', 'tablet_page_area' => '100', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>               
            </div>
        </div>
    <?php endif; ?>


        <?php if( $text['title'] || $text['subtitle'] || $text['caption'] ): ?>
            <div class="content <?= $block['align_text'] ? 'text-' . $block['align_text'] : '' ?>">
                <header>
                    <?= $text['title']    ? '<h2  >'     . $text['title']    .'</h1>' : '' ?>
                    <?= $text['subtitle'] ? '<p class="subtitle">'  . $text['subtitle'] .'</p>'  : '' ?>
                </header>
                <?= $text['caption']  ? '<p class="content">'   . $text['caption']  .'</p>'  : '' ?>
                <?php if( $buttons ): ?>
                    <div class="buttons theme--default <?= $block['align_text'] ? 'flex justify-' . $block['align_text'] : '' ?>">
                    <?php foreach( $buttons as $button ): $button = $button['button']?>
                        <?php $button['external'] ? $button['external'] = ' target="_blank" rel="noopener" ' : '' ?>
                        <a href="<?= $button['button_link'] ?>" <?= $button['external'] ?>class="button <?= $button['button_style'] ?>"><?= $button['button_text'] ?></a>
                    <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>

        <?php endif; ?>
            
            <div class="contact-form ">
                <?= $contact_form ?>
            </div>

            <div class="contact-info">
                <?php if($address || $county || $email || $tel || $post_code): ?>
                    <div class="contact-details flex flex-col footer-menu">
                        <h2 class="h5 font-primary">Contact Details</h2>
                        <p class="address"><?= $address ?></p>
                        <span class="flex">
                            <p class="county"><?= $county ?></p>,&nbsp;
                            <p class="post-code"> <?= $post_code ?></p>
                        </span>
                        <p class="tel">T: <a href="tel:<?= $tel ?>"> <?= $tel ?> </a></p>
                        <p class="tel">E: <a href="mailto:<?= $email ?>"> <?= $email ?> </a></p>
                    </div>
                <?php endif; ?>
            </div>

    </section>
    </section>

<?php } ?>