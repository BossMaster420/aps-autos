(function($) {

    if (this.childElementCount > 1) {
        $(this).slick({
            arrows: false,
            infinite: true,
            adaptiveHeight: false,
            speed: 300,
            cssEase: 'cubic-bezier(0, 0, 0.04, 0.98)',
            focusOnSelect: false,
            pauseOnHover: false,
            fade: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            dots: false,
        });
    }

    var textImageObjs = $('.text-image .images');

    textImageObjs.each(function(){
        if (this.childElementCount > 1) {
            $(this).slick({
                arrows: false,
                infinite: true,
                adaptiveHeight: false,
                speed: 600,
                cssEase: 'cubic-bezier(0, 0, 0.04, 0.98)',
                focusOnSelect: false,
                pauseOnHover: false,
                fade: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 5000,
                rows: 0,
                dots: true,
            });
        }
    });
    
})( jQuery );