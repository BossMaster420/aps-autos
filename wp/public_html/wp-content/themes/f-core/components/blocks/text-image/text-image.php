<?php 

/**
 * Text - Image
 *
 * This is the template that displays the 'Text - Image' block.
 */

acf_register_block_type(array(
    'name'              => 'Text / Image',
    'title'             => __('Text / Image'),
    'description'       => __('Text & Content Block'),
    'render_callback'   => 'text_image_render_callback',
    'category'          => 'fcore-blocks',
    'icon'              => 'align-pull-right',
    'keywords'          => array( 'text', 'image', 'content', 'text image' ),
    'supports'          => [ 'align' => false, 'align_text' => true ]
));


function text_image_render_callback( $block, $content = '', $is_preview = false, $post_id = 0 ) {

    $block_name     = 'text-image'; //Make Sure its the same as folder name
    $text           =  get_field( 'content_text' );
    // ->$text['title']
    // ->$text['subtitle']
    // ->$text['caption']
    $theme          = get_field( 'theme_select_theme_select' );
    // ->$theme['theme']
    // ->$theme['video']
    // ->$theme['image']
    // ->-> $theme['image']
    $image_type     = get_field( 'image_type' );
    $images         = get_field( 'images' );
    $content_order  = get_field( 'order' );

    $buttons        = get_field( 'buttons_buttons' );

    //Generate Names & ID's
    $class_name  = $block_name .' ';
    $class_name .= $image_type . ' ';
    $class_name .= $content_order . ' ';
    $class_name .= 'container ';
   ?>
   <section class="block <?= $theme['theme'] ?> ">
        <div class="<?= esc_attr($class_name) ?>">
        <?php if(DEBUG_MODE): ?>

            <?php wp_enqueue_style( 'block-acf-' . $block_name , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>

        <?php else: ?>

            <?php if( !is_admin() && $block['id'] === get_first_block_id() || !is_admin() && $block['id'] === get_second_block_id() ): ?>
                <style><?php echo file_get_contents(get_template_directory() . '/assets/css/'.$block_name.'/'.$block_name.'.css'); ?></style>
            <?php else: ?>
                <?php wp_enqueue_style( 'block-acf-'.$block_name.'-block' , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>
            <?php endif; ?>

        <?php endif; ?>

        <?php if( $theme['theme'] == 'theme--image' ): ?>
            <div class="bg-images-wrapper">
                <div class="bg-images">
                    <?php echo img_sizes($theme['image'], ['default' => 'img_1920', 'page_area' => '100', 'tablet_page_area' => '100', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>               
                </div>
            </div>
        <?php endif; ?>

        <?php if( $text['title'] || $text['subtitle'] || $text['caption'] ): ?>
            <div class="content <?= $block['align_text'] ? 'text-' . $block['align_text'] : '' ?>">
                <header data-aos="fade-up">
                    <?= $text['title']    ? '<h2      data-aos="fade-up" data-aos-delay="0">'  . $text['title']    .'</h1>' : '' ?>
                    <?= $text['subtitle'] ? '<p class="subtitle" data-aos="fade-up" data-aos-delay="100">'  . $text['subtitle'] .'</p>'  : '' ?>
                </header>
                <?= $text['caption']  ? '<div class="caption" data-aos="fade-up" data-aos-delay="200" >'   . $text['caption']  .'</div>'  : '' ?>
                <?php if( $buttons ): ?>
                    <div class="buttons <?= $block['align_text'] ? 'flex justify-' . $block['align_text'] : '' ?>">
                    <?php $delay = 250 ?>
                    <?php foreach( $buttons as $button ): $button = $button['button']?>
                        <?php $button['external'] ? $button['external'] = ' target="_blank" rel="noopener" ' : '' ?>
                        <a href="<?= $button['button_link'] ?>" <?= $button['external'] ?>class="button <?= $button['button_style'] ?>" data-aos="fade-up" data-aos-delay="<?= $delay; $delay += 50 ?>"><?= $button['button_text'] ?></a>
                    <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <?php if( $images ): ?>
        <div class="images-wrapper">
            <div class="images">
                <?php foreach( $images as $image ): $image = $image['image'] ?>
                    <?php echo img_sizes($image, ['default' => 'img_1920', 'page_area' => '40', 'tablet_page_area' => '100', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>
                <?php endforeach; ?>
            </div>
        </div>
        <?php endif; ?>

        </div>
    </section>

<?php } ?>