
(function($) {
    $galleryModal = $('.images-modal');
    $gallery = $('.js-gallery-slider');

    $gallery.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide, prevSlide){
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $('.js-modal-gallery-counter').html(i + '<span class="slash">/</span>' + slick.slideCount);
    });

    $('.js-modal-gallery-trigger').click(function(e) {
        e.preventDefault();
        $galleryModal.fadeIn();
        
        imgCount = $(this).attr('data-count');
        $gallery.slick('slickGoTo', parseInt(imgCount) );
        $('body').addClass('modal-active');

    });

    $('.js-modal-gallery-exit').click(function(e) {
        e.preventDefault();
        $galleryModal.fadeOut();
        $('body').removeClass('modal-active')
    });

    if( !$gallery.hasClass('slick-initialized') ) {
        $gallery.slick({
            arrows: true,
            dots: true,
            infinite: true,
            adaptiveHeight: false,
            speed: 300,
            cssEase: 'cubic-bezier(0, 0, 0.04, 0.98)',
            focusOnSelect: false,
            fade: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            prevArrow: '<button class="slick-prev slick-arrow transparent primary" aria-label="Next" type="button" style=""> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M14.842 17.685L9.158 12 14.842 6.315"/></svg></button>',
            nextArrow: '<button class="slick-next slick-arrow transparent primary" aria-label="Next" type="button" style=""> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9.158 6.315L14.842 12 9.158 17.685"/></svg></button>',
        });
    }

    $activeCategory = null;
    $('.js-category-nav a').click(function(e) {
        e.preventDefault()
        $category = $(this).attr('data-cat');

        if ($category == 'all' && $activeCategory != $category) {
            $('.image-grid').children().fadeOut();
            $('.image-grid').children().fadeIn();
        } 
        if($category != $activeCategory && $category != 'all') {
            $('.image-grid').children().fadeOut();
            $('.image-grid').children().filter('[data-cat="'+ $category +'"]').fadeIn();
        }
        $activeCategory = $category

    })
    //$('.js-modal-gallery-counter').html(i + '<span class="slash">/</span>' + slick.slideCount);
})( jQuery );