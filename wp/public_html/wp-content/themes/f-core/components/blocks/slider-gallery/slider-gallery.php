<?php 

/**
 * Gallery
 *
 * This is the template that displays the 'Gallery' block.
 * 
 * Occuraces to change
 * Slider Gallery
 * slider-gallery
 * slider_gallery
 */

acf_register_block_type(array(
    'name'              => 'Slider Gallery',
    'title'             => __('Slider Gallery'),
    'description'       => __('Slider Gallery Block'),
    'render_callback'   => 'slider_gallery_render_callback',
    'category'          => 'fcore-blocks',
    'icon'              => 'align-full-width',
    'keywords'          => array( 'cta', 'block', 'call', 'to', 'action', 'full', 'width', 'slider', 'gallery' ),
    'supports'          => [ 'align' => false, 'align_text' => true ]
));


function slider_gallery_render_callback( $block, $content = '', $is_preview = false, $post_id = 0 ) {
    /////////////////////////////////////////////////////////////////////////////
    // Base ACF Fields
    $block_name     = 'slider-gallery'; //Make Sure its the same as folder name
    $theme          =  get_field( 'theme_select_theme_select' );
    //////////////////////////////////////////////////////////////////////////////////
    // Block Related ACF Fields
    $gallery           =  get_field( 'gallery' );
    //////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////
    // Base Classes & Generate Names & ID's
    $class_name  = $block_name .' ';
    $class_name .= $theme['theme'] . ' ';
    $class_name .= 'container ';
    //////////////////////////////////////////////////////////////////////////////////
    // Block Related Classes

    //////////////////////////////////////////////////////////////////////////////////
   ?>
   <section class="block <?= $theme['theme'] ?> ">
    <section class="<?= esc_attr($class_name) ?>" data-aos="fade-up">

    <?php if(DEBUG_MODE): ?>

        <?php wp_enqueue_style( 'block-acf-' . $block_name , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>

    <?php else: ?>

        <?php if( !is_admin() && $block['id'] === get_first_block_id() || !is_admin() && $block['id'] === get_second_block_id() ): ?>
            <style><?php echo file_get_contents(get_template_directory() . '/assets/css/'.$block_name.'/'.$block_name.'.css'); ?></style>
        <?php else: ?>
            <?php wp_enqueue_style( 'block-acf-'.$block_name.'-block' , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>
        <?php endif; ?>

    <?php endif; ?>

    <?php if( $theme['theme'] == 'theme--image' ): ?>
        <div class="bg-images-wrapper">
            <div class="bg-images">
                <?php echo img_sizes($theme['image'], ['default' => 'img_1920', 'page_area' => '100', 'tablet_page_area' => '100', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>               
            </div>
        </div>
    <?php endif; ?>

        <div class="js-gallery-slider smoke-bottom" data-count="<?= $galleryCount; $galleryCount ++; ?>">
            <?php 
            $gallery_photos = get_field('gallery');
            foreach( $gallery_photos as $photo ): ?>
            
            <div class="img">
                <?php
                echo img_sizes($photo, ['default' => 'img_1920', 'page_area' => '100', 'tablet_page_area' => '100', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>               
                <div class="caption">
                    <?= get_the_title($photo); ?>
                </div>
            </div>

            <?php 
            endforeach; ?>
            </div>
            <span class="js-modal-gallery-counter modal-gallery-counter"></span>
        </div>


    </section>
    </section>

<?php } ?>