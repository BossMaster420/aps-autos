<?php 

/**
 * CTA Blocks
 *
 * This is the template that displays the 'CTA Blocks' block.
 */

acf_register_block_type(array(
    'name'              => 'CTA Blocks',
    'title'             => __('CTA Blocks'),
    'description'       => __('Text & Content Block'),
    'render_callback'   => 'cta_blocks_render_callback',
    'category'          => 'fcore-blocks',
    'icon'              => 'columns',
    'keywords'          => array( 'cta', 'blocks', 'call', 'to', 'action' ),
    'supports'          => [ 'align' => false, 'align_text' => true ]
));


function cta_blocks_render_callback( $block, $content = '', $is_preview = false, $post_id = 0 ) {
    /////////////////////////////////////////////////////////////////////////////
    // Base ACF Fields
    $block_name     = 'cta-blocks'; //Make Sure its the same as folder name
    $theme          =  get_field( 'theme_select_theme_select' );
    //////////////////////////////////////////////////////////////////////////////////
    // Block Related ACF Fields
    $text           =  get_field( 'content_text' );
    $ctas           = get_field( 'cta_blocks' );
    //////////////////////////////////////////////////////////////////////////////////


    $buttons        = get_field( 'buttons_buttons' );
    //////////////////////////////////////////////////////////////////////////////////
    // Base Classes & Generate Names & ID's
    $class_name  = $block_name .' ';
    $class_name .= $theme['theme'] . ' ';
    $class_name .= 'container ';
    //////////////////////////////////////////////////////////////////////////////////
    // Block Related Classes

    //////////////////////////////////////////////////////////////////////////////////
   ?>
   <section class="block <?= $theme['theme'] ?> ">
    <section class="<?= esc_attr($class_name) ?>">

    <?php if(DEBUG_MODE): ?>

        <?php wp_enqueue_style( 'block-acf-' . $block_name , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>

    <?php else: ?>

        <?php if( !is_admin() && $block['id'] === get_first_block_id() || !is_admin() && $block['id'] === get_second_block_id() ): ?>
            <style><?php echo file_get_contents(get_template_directory() . '/assets/css/'.$block_name.'/'.$block_name.'.css'); ?></style>
        <?php else: ?>
            <?php wp_enqueue_style( 'block-acf-'.$block_name.'-block' , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>
        <?php endif; ?>

    <?php endif; ?>

    <?php if( $theme['theme'] == 'theme--image' ): ?>
        <div class="bg-images-wrapper">
            <div class="bg-images">
                <?php echo img_sizes($theme['image'], ['default' => 'img_1920', 'page_area' => '100', 'tablet_page_area' => '100', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>               
            </div>
        </div>
    <?php endif; ?>

    <?php if( $text['title'] || $text['subtitle'] || $text['caption'] ): ?>
        <div class="content <?= $block['align_text'] ? 'text-' . $block['align_text'] : '' ?>">
            <header class="text-center">
                <?= $text['title']    ? '<h2   data-aos="fade-up" data-aos-delay="0">'     . $text['title']    .'</h1>' : '' ?>
                <?= $text['subtitle'] ? '<p class="subtitle" data-aos="fade-up" data-aos-delay="50">'  . $text['subtitle'] .'</p>'  : '' ?>
            </header>
            <?= $text['caption']  ? '<p class="content">'   . $text['caption']  .'</p>'  : '' ?>
            <?php if( $buttons ): ?>
                <div class="buttons <?= $block['align_text'] ? 'flex justify-' . $block['align_text'] : '' ?>">
                <?php foreach( $buttons as $button ): $button = $button['button']?>
                    <?php $button['external'] ? $button['external'] = ' target="_blank" rel="noopener" ' : '' ?>
                    <a href="<?= $button['button_link'] ?>" <?= $button['external'] ?>class="button <?= $button['button_style'] ?>"><?= $button['button_text'] ?></a>
                    <?php var_dump( $button ) ?>
                    dasdasdasdasasdas
                <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <div class="cta-blocks-wrapper flex content-center items-center justify-center flex-wrap">
        <?php $delay = 100 ?>
        <?php foreach($ctas as $cta): $cta = $cta['cta_block']; $button = $cta['button_button']; ?>
        <div class="cta-wrapper" data-aos="fade-up" data-aos-delay="<?= $delay; $delay += 100 ?>">
            <?php echo img_sizes($cta['image'], ['default' => 'img_800', 'page_area' => '25', 'tablet_page_area' => '25', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>
            <div class="cta text-center flex content-center items-center theme--image">
                    <h3 class="h2 no-margin"> <?= $cta['title'] ?>
                        <p class="caption"><?= $cta['caption'] ?></p>
                    </h3>
                    <a href="<?= $button['button_link'] ?>" <?= $button['external'] ? $button['external'] = ' target="_blank" rel="noopener" ' : '' ?> class="button no-margin <?= $button['button_style'] ?> theme--default"><?= $button['button_text'] ?></a>

                </div>
            </div>
        <?php endforeach; ?>
    </div>

    </section>
    </section>

<?php } ?>