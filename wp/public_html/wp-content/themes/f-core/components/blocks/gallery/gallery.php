<?php 

/**
 * Gallery
 *
 * This is the template that displays the 'Gallery' block.
 * 
 * Occuraces to change
 * Gallery
 * gallery
 * gallery
 */

acf_register_block_type(array(
    'name'              => 'Gallery',
    'title'             => __('Gallery'),
    'description'       => __('Gallery Block'),
    'render_callback'   => 'gallery_render_callback',
    'category'          => 'fcore-blocks',
    'icon'              => 'align-full-width',
    'keywords'          => array( 'cta', 'block', 'call', 'to', 'action', 'full', 'width' ),
    'supports'          => [ 'align' => false, 'align_text' => true ]
));


function gallery_render_callback( $block, $content = '', $is_preview = false, $post_id = 0 ) {
    /////////////////////////////////////////////////////////////////////////////
    // Base ACF Fields
    $block_name     = 'gallery'; //Make Sure its the same as folder name
    $theme          =  get_field( 'theme_select_theme_select' );
    //////////////////////////////////////////////////////////////////////////////////
    // Block Related ACF Fields
    //////////////////////////////////////////////////////////////////////////////////

    $buttons        = get_field( 'buttons_buttons' );
    //////////////////////////////////////////////////////////////////////////////////
    // Base Classes & Generate Names & ID's
    $class_name  = $block_name .' ';
    $class_name .= 'container ';
    //////////////////////////////////////////////////////////////////////////////////
    // Block Related Classes

    //////////////////////////////////////////////////////////////////////////////////
   ?>
   <section class="block <?= $theme['theme'] ?> ">
    <section class="<?= esc_attr($class_name) ?>" data-aos="fade-up">

    <?php if(DEBUG_MODE): ?>

        <?php wp_enqueue_style( 'block-acf-' . $block_name , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>

    <?php else: ?>

        <?php if( !is_admin() && $block['id'] === get_first_block_id() || !is_admin() && $block['id'] === get_second_block_id() ): ?>
            <style><?php echo file_get_contents(get_template_directory() . '/assets/css/'.$block_name.'/'.$block_name.'.css'); ?></style>
        <?php else: ?>
            <?php wp_enqueue_style( 'block-acf-'.$block_name.'-block' , get_template_directory_uri() . '/assets/css/'.$block_name.'/'.$block_name.'.css' ); ?>
        <?php endif; ?>

    <?php endif; ?>

    <?php if( $theme['theme'] == 'theme--image' ): ?>
        <div class="bg-images-wrapper">
            <div class="bg-images">
                <?php echo img_sizes($theme['image'], ['default' => 'img_1920', 'page_area' => '100', 'tablet_page_area' => '100', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>               
            </div>
        </div>
    <?php endif; ?>

        <div class="in-page-nav-wrap mb-8">
            <div class="in-page-nav js-category-nav flex flex-wrap justify-center text-center">
                <a href="#" class="active" data-cat="all">All</a>
                <?php
                $args = array(
                    'post_type' => 'gallery',
                    'posts_per_page' => -1
                );
                $loop = new WP_Query( $args ); ?>

                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <a href="#" data-cat="<?php echo get_post_field( 'post_name', get_post() ); ?>"><?php the_title(); ?></a>
                <?php endwhile; wp_reset_query(); ?>
            </div>
        </div>

        <div class="image-grid flex justify-center flex-wrap">
            <?php 
            
            $args = [
                'post_type' => 'gallery',
                'posts_per_page' => -1
            ]

            ?>
            <?php $gallery_query = new WP_Query($args) ?>

            <?php 
            if ( $gallery_query -> have_posts() ) :
                $galleryCount = 0;
                while ( $gallery_query -> have_posts() ) : $gallery_query -> the_post(); 
                    $gallery_id = get_the_ID();
                    $gallery_photos = get_field('gallery', $gallery_id);
                    $delay = 0;
                    foreach( $gallery_photos as $photo ): ?>
                        <div class="img-wrapper" data-cat="<?php echo get_post_field( 'post_name', get_post() ); ?>" data-aos="fade-up" data-aos-delay="<?= $delay; $delay += 100 ?>">
                        <?php 
                            if ($delay > 149) {
                                $delay = 0;
                            }
                        ?>
                            <div class="dummy"></div>
                            <a href="#"class="img js-modal-gallery-trigger" data-count="<?= $galleryCount; $galleryCount ++; ?>">
                                <?php
                                echo img_sizes($photo, ['default' => 'img_1920', 'page_area' => '100', 'tablet_page_area' => '100', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit']); ?>               
                            </a>
                        </div>

                    <?php 
                    endforeach;
                endwhile;
            endif;
            ?>

        </div>

        <div class="images-modal smoke-top">
            <?php 
            
            $args = [
                'post_type' => 'gallery',
                'posts_per_page' => -1
            ]

            ?>
            <?php $gallery_query = new WP_Query($args) ?>

            <div class="js-modal-gallery modal-gallery">
            <?php 
            if ( $gallery_query -> have_posts() ) :
                $galleryCount = 0; ?>
                <?php
                while ( $gallery_query -> have_posts() ) : $gallery_query -> the_post(); 
                $gallery_id = get_the_ID();
                $gallery_photos = get_field('gallery', $gallery_id);
                foreach( $gallery_photos as $photo ): ?>

                    <div href="#" class="img-slide" data-count="<?= $galleryCount; $galleryCount ++; ?>">
                        <?php
                        $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
                        $image_title = get_the_title($image_id);
                        echo img_sizes($photo, ['default' => 'img_1920', 'page_area' => '100', 'tablet_page_area' => '100', 'mobile_page_area' => '100', 'lazy_load' => true, 'class' => 'object-fit contain']); ?>               
                        <div class="dummy js-modal-gallery-exit"></div>
                        <h3><?= $title ?></h3>
                        <p class="caption"><?= $alt ?></p>
                    </div>

                    <?php 
                    endforeach;
                endwhile; ?>
            <?php 
            endif;
            ?>
            </div>
            <span class="js-modal-gallery-counter modal-gallery-counter"></span>
            <a href="#" class="js-modal-gallery-exit modal-exit">
                <div class="exit">
                    <span></span>
                    <span></span>
                </div>
            </a>

        </div>


    </section>

<?php } ?>