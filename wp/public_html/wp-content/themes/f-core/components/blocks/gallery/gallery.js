(function($) {
    $galleryModal = $('.images-modal');
    $gallery = $('.js-modal-gallery');

    $gallery.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide, prevSlide){
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $('.js-modal-gallery-counter').html(i + '<span class="slash">/</span>' + slick.slideCount);
    });

    $('.js-modal-gallery-trigger').click(function(e) {
        e.preventDefault();
        $galleryModal.fadeIn();
        
        imgCount = $(this).attr('data-count');
        $('.js-modal-gallery').slick('slickGoTo', parseInt(imgCount) );
        $('body').addClass('modal-active');

    });

    $('.js-modal-gallery-exit').click(function(e) { 
        e.preventDefault();
        $galleryModal.fadeOut();
        $('body').removeClass('modal-active')
    });

    if( !$gallery.hasClass('slick-initialized') ) { 
        $gallery.slick({
            arrows: true,
            infinite: false,
            adaptiveHeight: false,
            speed: 300,
            cssEase: 'cubic-bezier(0, 0, 0.04, 0.98)',
            focusOnSelect: false,
            fade: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            nextArrow: '<button class="slick-next slick-arrow transparent primary" aria-label="Next" type="button" style=""> <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 48 48"><title>arrow-right</title><g class="arrow-right"><line class="arrow-stem" x1="39.964" y1="23.964" x2="7.964" y2="23.964" stroke-width="3" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" fill="none"/><polyline class="arrowhead" points="28 11.929 40.036 23.964 28 36" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/></g></svg></button>',
            prevArrow: '<button class="slick-prev slick-arrow transparent primary" aria-label="Next" type="button" style=""> <svg width="28" height="28" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><title>arrow-left</title><g class="arrow-right"><line class="arrow-stem" x1="8.036" y1="23.964" x2="40.036" y2="23.964" stroke-width="3" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" fill="none"/><polyline class="arrowhead" points="20 36 7.964 23.964 20 11.929" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/></g></svg></button>',
        });
    }

    $activeCategory = null;
    $('.js-category-nav a').on('click', function(e) {
        AOS.refresh();
        e.preventDefault()
        $category = $(this).attr('data-cat');
        $(this).parent().children().removeClass('active');
        $(this).addClass('active');

        if($category != $activeCategory) {
            updateGallery($category);
        }

        $activeCategory = $category;
    })

function updateGallery($category) {
    $('.image-grid').children().fadeOut( 'fast', 'swing', function() { fadeInGallery($category) } );

    function fadeInGallery($category) {
        if($category == 'all') {
            $('.image-grid').children().fadeIn( 'fast', 'swing', function(){ AOS.refresh(); } );
        } else {
            $('.image-grid').children().filter('[data-cat="'+ $category +'"]').fadeIn('fast', 'swing', function(){ AOS.refresh(); } );
        }
    }
}

})( jQuery );