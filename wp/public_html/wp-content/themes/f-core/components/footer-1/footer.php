<?php

$address = get_field('address', 'options');
$county = get_field('county', 'options');
$post_code = get_field('post_code', 'options');
$tel = get_field('tel', 'options');
$email = get_field('email', 'options');

$facebook_link  = get_field('facebook', 'options');
$instagram_link = get_field('instagram', 'options');
$twitter_link   = get_field('twitter', 'options');
$linkedin_link  = get_field('linked_in', 'options');
?>

<footer class="footer-1 container">

    <div class="footer-top">
        <div class="footer-menus flex flex-wrap">
            
            <div class="footer-menu menu-1"><?php wp_nav_menu( array( 'theme_location' => 'Footer Menu 1' , 'container' => false, 'menu_class' => 'list-reset' ) ); ?></div>
            
            <div class="footer-menu menu-2"><?php wp_nav_menu( array( 'theme_location' => 'Footer Menu 2' , 'container' => false, 'menu_class' => 'list-reset' ) ); ?></div>

            <div class="footer-menu menu-3"><?php wp_nav_menu( array( 'theme_location' => 'Footer Menu 3' , 'container' => false, 'menu_class' => 'list-reset' ) ); ?></div>

            <?php if($address || $county || $email || $tel || $post_code): ?>
            <div class="contact-details flex flex-col footer-menu">
                <h2 class="h5 font-primary">Contact Details</h2>
                <p class="address"><?= $address ?></p>
                <span class="flex">
                    <p class="county"><?= $county ?></p>,&nbsp;
                    <p class="post-code"> <?= $post_code ?></p>
                </span>
                <p class="tel">T: <a href="tel:<?= $tel ?>"> <?= $tel ?> </a></p>
                <p class="tel">E: <a href="mailto:<?= $email ?>"> <?= $email ?> </a></p>
            </div>
            <?php endif; ?>
        </div>


        <div class="social-links flex">
        <?php if( $facebook_link ): ?>
            <a href="<?= $facebook_link; ?>" class="button icon secondary size-s" target="_blank" rel="noopener">
                <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><title>facebook</title><path class="facebook" d="M34.107,3.567a45.739,45.739,0,0,0-5.334-.281c-5.288,0-8.914,3.228-8.914,9.148v5.1H13.893v6.925h5.966V42.216h7.159V24.459H32.96l.913-6.925H27.018V13.112c0-1.989.538-3.369,3.416-3.369h3.673Z" fill="currentColor"/></svg>
            </a>
        <?php endif; ?>
        <?php if( $instagram_link ): ?>
            <a href="<?= $instagram_link; ?>" class="button icon secondary size-s" target="_blank" rel="noopener">
                <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><title>instagram</title><path d="M42.12,16.472c-.143-2.921-.808-5.51-2.945-7.647s-4.726-2.8-7.647-2.945c-2.517-.142-5.011-.119-7.528-.119s-5.011-.023-7.528.119c-2.921.143-5.51.808-7.647,2.945s-2.8,4.726-2.945,7.647c-.142,2.517-.119,5.011-.119,7.528s-.023,5.011.119,7.528c.143,2.921.808,5.51,2.945,7.647s4.726,2.8,7.647,2.945c2.517.142,5.011.119,7.528.119s5.011.023,7.528-.119c2.921-.143,5.51-.808,7.647-2.945s2.8-4.726,2.945-7.647c.142-2.517.119-5.011.119-7.528S42.262,18.989,42.12,16.472ZM38.225,34.758a6.212,6.212,0,0,1-3.467,3.467c-2.4.95-8.1.736-10.758.736s-8.359.214-10.758-.736a6.212,6.212,0,0,1-3.467-3.467c-.95-2.4-.736-8.1-.736-10.758s-.214-8.359.736-10.758a6.212,6.212,0,0,1,3.467-3.467c2.4-.95,8.1-.736,10.758-.736s8.359-.214,10.758.736a6.212,6.212,0,0,1,3.467,3.467c.95,2.4.736,8.1.736,10.758S39.175,32.359,38.225,34.758Z" fill="currentColor"/><path d="M33.737,12.078a2.185,2.185,0,1,0,2.185,2.185A2.18,2.18,0,0,0,33.737,12.078Z" fill="currentColor"/><path d="M24,14.643A9.357,9.357,0,1,0,33.357,24,9.345,9.345,0,0,0,24,14.643ZM24,30.08A6.08,6.08,0,1,1,30.08,24,6.092,6.092,0,0,1,24,30.08Z" fill="currentColor"/></svg>
            </a>
        <?php endif; ?>
        <?php if( $twitter_link ): ?>
            <a href="<?= $twitter_link; ?>" class="button icon secondary size-s" target="_blank" rel="noopener">
                <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><title>twitter</title><path class="twitter" d="M43.457,13.233a15.624,15.624,0,0,1-4.485,1.206A7.763,7.763,0,0,0,42.4,10.147a15.348,15.348,0,0,1-4.943,1.881,7.8,7.8,0,0,0-13.478,5.329,8.765,8.765,0,0,0,.193,1.784,22.14,22.14,0,0,1-16.059-8.15A7.8,7.8,0,0,0,10.52,21.407,7.849,7.849,0,0,1,7,20.419v.1a7.789,7.789,0,0,0,6.245,7.644,8.269,8.269,0,0,1-2.05.265,9.718,9.718,0,0,1-1.47-.121,7.8,7.8,0,0,0,7.281,5.4,15.6,15.6,0,0,1-9.668,3.328,15.963,15.963,0,0,1-1.881-.1,22,22,0,0,0,11.959,3.5c14.323,0,22.159-11.862,22.159-22.158,0-.338,0-.675-.024-1.013A16.728,16.728,0,0,0,43.457,13.233Z" fill="currentColor"/></svg>
            </a>
        <?php endif; ?>
        <?php if( $linkedin_link ): ?>
            <a href="<?= $linkedin_link; ?>" class="button icon secondary size-s" target="_blank" rel="noopener">
                <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><title>linkedin</title><g class="linkedin"><rect x="7.435" y="17.44" width="7.298" height="21.916" fill="currentColor"/><path d="M15.2,10.673a3.763,3.763,0,0,0-4.069-3.782,3.8,3.8,0,0,0-4.114,3.782,3.762,3.762,0,0,0,4.025,3.781h.045A3.777,3.777,0,0,0,15.2,10.673Z" fill="currentColor"/><path d="M40.985,26.8c0-6.723-3.583-9.864-8.382-9.864a7.222,7.222,0,0,0-6.613,3.694h.045V17.44H18.759s.088,2.057,0,21.916h7.276V27.127a5.489,5.489,0,0,1,.243-1.792,3.988,3.988,0,0,1,3.737-2.654c2.632,0,3.694,2.013,3.694,4.954V39.356h7.276Z" fill="currentColor"/></g></svg>
            </a>
        <?php endif; ?>
    </div>
    <div class="footer-bottom flex justify-between">
        <div class="left"><?php wp_nav_menu( array( 'theme_location' => 'Copyright' , 'container' => false, 'menu_class' => 'list-reset' ) ); ?></div>
        <div class="right">©2021 Fiducia</div>
    </div>

</footer>