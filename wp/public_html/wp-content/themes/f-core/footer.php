<?php include_header_footer('footer') ?>

<!-- Scripts & Libs -->

<link href="<?php echo get_template_directory_uri(); ?>/assets/css/global.css" rel="stylesheet">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/blocks.min.js"></script>

<?php if(DEBUG_MODE) : ?>
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/abovethefold.css" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri(); ?>/core/js/main.js"></script>
<?php endif; ?>

<?php if(!DEBUG_MODE) : ?>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.min.js"></script>
<?php endif; ?>

<?php wp_footer();?>